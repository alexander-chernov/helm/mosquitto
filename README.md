# DDClient

Eclipse Mosquitto is an open source message broker which implements MQTT version 5, 3.1.1 and 3.1

![Version: 0.2.0](https://img.shields.io/badge/Version-0.2.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 2.0.19](https://img.shields.io/badge/AppVersion-2.0.19-informational?style=flat-square)

## Additional Information

## Installing the Chart

To install the chart with the release name `my-release`:

```console
$ helm repo add alekc-charts https://charts.alekc.dev/
$ helm install my-release alekc-charts/mosquitto
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"eclipse-mosquitto"` |  |
| image.tag | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| liveness.enabled | bool | `true` | If true enables liveness probe |
| mosquitto.acls | string | `""` |  |
| mosquitto.config | string | `""` | Additional config |
| mosquitto.persistence.accessModes[0] | string | `"ReadWriteOnce"` |  |
| mosquitto.persistence.enabled | bool | `false` |  |
| mosquitto.persistence.matchExpressions | object | `{}` |  |
| mosquitto.persistence.matchLabels | object | `{}` |  |
| mosquitto.persistence.path | string | `"/mosquitto/data/"` |  |
| mosquitto.persistence.size | string | `"100Mi"` |  |
| mosquitto.persistence.subPath | string | `""` |  |
| mosquitto.ssl.certificate.dnsNames | list | `[]` |  |
| mosquitto.ssl.certificate.enabled | bool | `false` |  |
| mosquitto.ssl.certificate.issuerRef.group | string | `"cert-manager.io"` |  |
| mosquitto.ssl.certificate.issuerRef.kind | string | `"ClusterIssuer"` |  |
| mosquitto.ssl.certificate.issuerRef.name | string | `"letsencrypt"` |  |
| mosquitto.ssl.enabled | bool | `false` |  |
| mosquitto.ssl.portName | string | `"mqttssl"` |  |
| mosquitto.ssl.secretName | string | `"mosquitto-ssl"` |  |
| mosquitto.users | string | `""` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.externalIp.enabled | bool | `false` | enables external ip service. Useful if you want to additionally expose some specific IP (i.e. lan) |
| service.externalIp.ips | list | `[]` | if external ips for the service |
| service.ports.mqtt | int | `1883` |  |
| service.ports.mqttssl | int | `8883` |  |
| service.ports.mqttws | int | `9001` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| tolerations | list | `[]` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.13.1](https://github.com/norwoodj/helm-docs/releases/v1.13.1)
